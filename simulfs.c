#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE *fileSystem;
FILE *fp;

struct rootdir_entry{
	char name[20];
	unsigned short int size;
	unsigned short int index;
};

struct rootdir{
	unsigned short int free_blocks;
	unsigned short int trailing;
	struct rootdir_entry list_entry[170];
	unsigned char reserved[12];
};

unsigned char sector_data[1024];

unsigned short int sector_index[512];

struct rootdir * root;


int filesize(FILE* fp){
	int curr = ftell(fp);
	int sz;
	fseek(fp, 0L, SEEK_END);
	sz = ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	return sz;
}

int getAddress(int index){
	int resp = (index-1) * 1024 + 4096;
	return resp;
}

int findByName(char fileName[]){
	int i;
	fileSystem = fopen("simul.fs", "rb");
	unsigned short int index = 0;
	fseek(fileSystem, 4, SEEK_SET);
	struct rootdir_entry entry ;
	for(i=0; i<170;i++){
		fread(&entry, 24, 1, fileSystem);
		if(strcmp(fileName, entry.name)==0){
			fclose(fileSystem);
			return entry.index;
		}
	}

	fclose(fileSystem);
	return 0;
}

int getEntrySize(char fileName[]){
	int i;
	for(i=0; i<170;i++){		
		if(strcmp(root->list_entry[i].name, fileName)==0){
			return root->list_entry[i].size;
		}
	}
	return 0;
}

void init(){
	root = malloc(sizeof(struct rootdir));	  
	memset(root, 0, sizeof(struct rootdir));
	root -> free_blocks = 1;

	fileSystem = fopen("simul.fs", "wb+");

	//grava struct cabecalho
	fwrite(root, sizeof(struct rootdir), 1, fileSystem);

	//grava root entry
	unsigned char array[1022];
	memset(array, 0, sizeof(char) * 1022);
	int i;
	unsigned short int num = 0;
	for(i=1; i<=65532; i++){
		num = i+1;
		if(i == 65532){
			num = 0;
		}
		fwrite(&num, sizeof(num), 1, fileSystem);
		fwrite(&array, sizeof(array), 1, fileSystem);
	}

	fclose(fileSystem);
}

void create(char fileName[]){
	memset(sector_index, 0, sizeof(short int) * 512);
	
	fp = fopen(fileName, "rb");
	int size = filesize(fp);

	int blocks = size / 1024;
	if(blocks >= 512){
		printf("Arquivo muito grande, falta indice \n");
		return;
	}

	struct rootdir_entry newEntry;
	strcpy(newEntry.name, fileName);
	newEntry.size = size;
	newEntry.index = root->free_blocks;

	fileSystem = fopen("simul.fs", "rb+");
	int i=0;	

	int currentAddress = getAddress(root->free_blocks);
	unsigned short int indexNextBlock;
	unsigned char array[1022];
	
	//le bloco de indice para encontrar primeiro bloco vazio
	fseek (fileSystem, currentAddress, SEEK_SET);
	fread(&indexNextBlock, sizeof(indexNextBlock), 1, fileSystem);
	fseek(fileSystem, 1022, SEEK_CUR);

	while(size > 0){
		memset(sector_data, 0, sizeof(sector_data));
		
		//le arquivo a ser gravado
		fread(&sector_data, sizeof(sector_data), 1, fp);

		//le indice proximo bloco livre
		sector_index[i] = indexNextBlock;
		currentAddress = getAddress(indexNextBlock);
		fseek (fileSystem, currentAddress, SEEK_SET);
		fread(&indexNextBlock, sizeof(indexNextBlock), 1, fileSystem);
		root->free_blocks = indexNextBlock;
		
		//grava dado no bloco
		fseek (fileSystem, currentAddress, SEEK_SET);
		fwrite(&sector_data, sizeof(sector_data), 1, fileSystem);		

		size -= 1024;
		i++;
	}

	//atualiza bloco de indice
	fseek(fileSystem, getAddress(newEntry.index), SEEK_SET);
	fwrite(&sector_index, sizeof(sector_index), 1, fileSystem);

	//atualiza tabela de arquivos
	for(i=0; i<170; i++){
		if(root->list_entry[i].index == 0){
			root->list_entry[i] = newEntry;
			printf("New entry\nindex:% d\nname: %s\nsize: %d \n", newEntry.index, newEntry.name, newEntry.size);
			break;
		}
	}

	fseek(fileSystem, 0, SEEK_SET);
	fwrite(root, sizeof(struct rootdir), 1, fileSystem);

	fclose(fileSystem);
	fclose(fp);
}

void read(char fileName[]){
	int index = findByName(fileName);
	if (index <= 0) {
		printf("Não existe\n");
		return;
	}
	printf("index: %d \n", index);
	printf("address: %d \n", getAddress(index));
	fileSystem = fopen("simul.fs", "rb+");
	fseek(fileSystem, getAddress(index), SEEK_SET);
	fread(&sector_index, sizeof(sector_index), 1, fileSystem);
	
	fp = fopen(fileName, "wb+");

	int i = 0;
	int size = getEntrySize(fileName);
	printf("SIZE %d\n", size);
	
	while(size > 0){
		fseek(fileSystem, getAddress(sector_index[i]), SEEK_SET);

		fread(&sector_data, sizeof(sector_data), 1, fileSystem);
		if(size<1024){
			fwrite(&sector_data, size, 1, fp);
		} else{
			fwrite(&sector_data, sizeof(sector_data), 1, fp);	
		}
		
		i++;
		size-=1024;
	}

	fclose(fp);
	fclose(fileSystem);
}

void delete(char fileName[]){
	int index = findByName(fileName);
	if (index <= 0) {
		printf("Não existe\n");
		return;
	}
	fileSystem = fopen("simul.fs", "rb+");
	fseek(fileSystem, getAddress(index), SEEK_SET);
	fread(&sector_index, sizeof(sector_index), 1, fileSystem);
	unsigned short int oldIndex = root->free_blocks;
	root->free_blocks = index;

	int i = 0;
	fseek(fileSystem, getAddress(index), SEEK_SET);
	fwrite(&sector_index[i], sizeof(unsigned short int), 1, fileSystem);
	
	while(sector_index[i] > 0 && i < 512){
		fseek(fileSystem, getAddress(sector_index[i]), SEEK_SET);
		if(i == 511 || sector_index[i+1] == 0){
			fwrite(&oldIndex, sizeof(unsigned short int), 1, fileSystem);
		} else{
			fwrite(&sector_index[i+1], sizeof(unsigned short int), 1, fileSystem);
		}
		
		i++;
	}

	for (i = 0; i < 170; ++i){
		if(strcmp(fileName, root->list_entry[i].name)==0){
			root->list_entry[i].index = 0;
			break;
		}
	}
	fseek(fileSystem, 0, SEEK_SET);
	fwrite(root, sizeof(struct rootdir), 1, fileSystem);

	fclose(fileSystem);
}

void list(){
	
	printf("Index \t\t Nome \t\t Size \n");
	struct rootdir_entry entry;
	int i;
	for(i=0; i < 170; i++){
		entry = root->list_entry[i];
		if(entry.index>0) {
			printf("entry: %d\t %s\t %d\n", entry.index, entry.name, entry.size);
		}
	}
}

int main(int argc, char **argv){
	if (argc == 1) {
		printf("Uso: simulfs [opção]\nOpção:\n-init\tformata o sistema de arquivo\n-ls\tlista o conteudo do simulfs\n-create [arquivo]\tcria um arquivo simulfs com nome [arquivo]\n-read [arquivo]\tle um arquivo simulfs e salva no disco\n-del [arquivo]\tapaga um arquivo simulfs\n\n");
	return 1;
	}

	//carrega root
	fileSystem = fopen("simul.fs", "rb+");
	if(fileSystem != NULL){
		root = malloc(4096);

		fread(root, 4096, 1, fileSystem);

		fclose(fileSystem);
	}

	if ((argc == 2) & strcmp("-ls", argv[1]) == 0) {
		list();
		return 0;
	} 
	else if ((argc == 2) & strcmp("-init", argv[1]) == 0) {
		init();
		return 0;
	}
	else if ((argc == 3) & strcmp("-create", argv[1]) == 0) {
		if (strlen(argv[2]) > 20) {
			printf("Nome muito grande.\n"); 
			return 0;
		}
		create(argv[2]);
		return 0;
	}
	else if ((argc == 3) & strcmp("-read", argv[1]) == 0) {
		read(argv[2]);
		return 0;
	}
	else if ((argc == 3) & strcmp("-del", argv[1]) == 0) {
		delete(argv[2]);
		return 0;
	}
}
